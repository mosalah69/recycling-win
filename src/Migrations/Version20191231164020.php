<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191231164020 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contenu (id INT AUTO_INCREMENT NOT NULL, bac_manager_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, matiere VARCHAR(255) NOT NULL, image_lien VARCHAR(255) NOT NULL, prix DOUBLE PRECISION NOT NULL, INDEX IDX_89C2003F4053FA97 (bac_manager_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, pseudo VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, image_lien VARCHAR(255) DEFAULT NULL, code_postal INT NOT NULL, ville VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bac_manager (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, total_points INT NOT NULL, actif TINYINT(1) NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, couleur VARCHAR(255) NOT NULL, image_lien VARCHAR(255) NOT NULL, INDEX IDX_D3BA5797A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE don (id INT AUTO_INCREMENT NOT NULL, partenaire_id INT DEFAULT NULL, montant DOUBLE PRECISION NOT NULL, INDEX IDX_F8F081D998DE13AC (partenaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partenaire (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, mot_passe VARCHAR(255) NOT NULL, nom_entreprise VARCHAR(255) NOT NULL, siret VARCHAR(255) NOT NULL, image_lien VARCHAR(255) DEFAULT NULL, telephone VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contenu ADD CONSTRAINT FK_89C2003F4053FA97 FOREIGN KEY (bac_manager_id) REFERENCES bac_manager (id)');
        $this->addSql('ALTER TABLE bac_manager ADD CONSTRAINT FK_D3BA5797A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE don ADD CONSTRAINT FK_F8F081D998DE13AC FOREIGN KEY (partenaire_id) REFERENCES partenaire (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bac_manager DROP FOREIGN KEY FK_D3BA5797A76ED395');
        $this->addSql('ALTER TABLE contenu DROP FOREIGN KEY FK_89C2003F4053FA97');
        $this->addSql('ALTER TABLE don DROP FOREIGN KEY FK_F8F081D998DE13AC');
        $this->addSql('DROP TABLE contenu');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE bac_manager');
        $this->addSql('DROP TABLE don');
        $this->addSql('DROP TABLE partenaire');
    }
}
