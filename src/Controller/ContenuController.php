<?php

namespace App\Controller;

use App\Entity\Contenu;
use App\Repository\ContenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ContenuController extends AbstractController
{
    /**
     * @Route("/contenu", name="contenu")
     */
    public function index(ContenuRepository $repo)
    {
        $contenu = $repo->findBy(["bacManager"=>null]);
        return $this->render('contenu/index.html.twig', [
            'contenus' => $contenu,
        ]);
    }

    /**
     * @Route("/results", name="search_results")
     */
    public function search_results(ContenuRepository $repo, Request $request)
    {
        $found = false;
        $search = $request->get('Search');
        $contenu = [];
        if ($search) {
            $contenu = $repo->findByKeyword($search);
            if ($contenu == []) {
                $found = true;
            }
        }

        return $this->render('contenu/list.html.twig', [
            'search' => $search,
            'contenu' => $contenu,
            'found' => $found
        ]);
    }
}
