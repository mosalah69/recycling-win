<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RecyclageController extends AbstractController
{
    /**
     * @Route("/recyclage", name="recyclage")
     */
    public function index()
    {
        return $this->render('recyclage/index.html.twig', [
            'controller_name' => 'RecyclageController',
        ]);
    }
}
