<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class QuiController extends AbstractController
{
    /**
     * @Route("/qui", name="qui")
     */
    public function index()
    {
        return $this->render('qui/index.html.twig', [
            'controller_name' => 'QuiController',
        ]);
    }
}
