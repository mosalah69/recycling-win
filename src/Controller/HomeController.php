<?php

namespace App\Controller;

use App\Repository\ContenuRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(ContenuRepository $repo)
    {
        $contenu = $repo->findBy(["bacManager"=>null]);

        return $this->render('home/index.html.twig', [
            'contenus' => $contenu,
        ]);

    }
 
}
