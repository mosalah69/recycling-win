<?php

namespace App\Controller;

use App\Entity\BacManager;
use App\Entity\Contenu;
use App\Repository\BacManagerRepository;
use App\Repository\ContenuRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class BacManagerController extends AbstractController
{
    /**
     * @Route("/bac", name="bac_manager")
     */
    public function index(BacManagerRepository $bacManagerRepository )
    {
        /** BacManager bacManager1 */
        $bacManager1 = $bacManagerRepository->findBy(array('actif' => true, 'user' => $this->getUser()));
        $total = 0 ;
        $image = [];
        $content = [];

        foreach ($bacManager1 as $bacs) {
            $total += $bacs->getTotal();
            $image = $bacs->getImageLien();
            /** Contenu $contenu */
            for ($i=0; $i < count($bacs->getContenu()); $i++) { 
               $image []= $bacs->getContenu()[$i]->getImageLien();
            }
            $content []=  $image;
        }
        return $this->render('bac_manager/index.html.twig', [
            'total' => $total,
            'bacs' => $bacManager1,
            'contents' =>$content
        ]);
    }

    /**
     * @Route("/bac/add/{id}", name="bac_add")
     */
    public function add(Contenu $contenuModel, EntityManagerInterface $entityManager, SessionInterface $session, BacManagerRepository $bacManagerRepository)
    {
        // Cette fonction me permet d'ajouter le produit dans le bon bac!
        $contenu = new Contenu();
        $contenu->setImageLien($contenuModel->getImageLien())->setMatiere($contenuModel->getMatiere())->setNom($contenuModel->getNom())->setPrix($contenuModel->getPrix());

        $bacJaune = $bacManagerRepository->findOneBy(array('actif' => true, 'user' => $this->getUser(), 'couleur' => 'jaune'));
        $bacVert = $bacManagerRepository->findOneBy(array('actif' => true, 'user' => $this->getUser(), 'couleur' => 'vert'));
        $bacGris = $bacManagerRepository->findOneBy(array('actif' => true, 'user' => $this->getUser(), 'couleur' => 'gris'));

        switch ($contenu->getMatiere()) {
            case 'Plastique':
                $bacJaune->addContenu($contenu);
                break;
            case 'Carton':
                $bacJaune->addContenu($contenu);
                break;
            case 'Verre':
                $bacVert->addContenu($contenu);
                break;
            default:
            case 'Déchets Ménagers':
                $bacGris->addContenu($contenu);
                break;
        }
        $entityManager->persist($contenu);
        $entityManager->flush();
        return $this->redirectToRoute('bac_manager');
    }
    /**
     *  @Route("/bac/supprimerPanier/{id}", name="supprimer_panier")
     */
    public function supprimerPanier($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $panier = $entityManager->getRepository(BacManager::class)->find($id);
        foreach ($panier->getContenu() as $contenu) {
            $entityManager->remove($contenu);
        }
        $entityManager->flush();
        return $this->redirectToRoute('bac_manager');
    }

}
