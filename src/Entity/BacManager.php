<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BacManagerRepository")
 */
class BacManager
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalPoints;

    /**
     * @ORM\Column(type="boolean")
     */
    private $actif;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateDebut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $couleur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageLien;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="BacManager")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contenu", mappedBy="bacManager")
     */
    private $Contenu;

    public function __construct()
    {
        $this->Contenu = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalPoints(): ?int
    {
        return $this->totalPoints;
    }

    public function setTotalPoints(int $totalPoints): self
    {
        $this->totalPoints = $totalPoints;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->DateDebut;
    }

    public function setDateDebut(\DateTimeInterface $DateDebut): self
    {
        $this->DateDebut = $DateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getImageLien(): ?string
    {
        return $this->imageLien;
    }

    public function setImageLien(string $imageLien): self
    {
        $this->imageLien = $imageLien;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Contenu[]
     */
    public function getContenu(): Collection
    {
        return $this->Contenu;
    }

    public function addContenu(Contenu $contenu): self
    {
        if (!$this->Contenu->contains($contenu)) {
            $this->Contenu[] = $contenu;
            $contenu->setBacManager($this);
        }

        return $this;
    }

    public function removeContenu(Contenu $contenu): self
    {
        if ($this->Contenu->contains($contenu)) {
            $this->Contenu->removeElement($contenu);
            // set the owning side to null (unless already changed)
            if ($contenu->getBacManager() === $this) {
                $contenu->setBacManager(null);
            }
        }

        return $this;
    }

    public function getTotal() {
        $total = 0;
        foreach ($this->getContenu() as $item) {

            $total += $item->getPrix();
          
        }
        return $total;
    }
}
