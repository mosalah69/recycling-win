<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageLien;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BacManager", mappedBy="user", cascade={"persist", "remove"})
     */
    private $BacManager;

    /**
     * @ORM\Column(type="integer")
     */
    private $CodePostal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Ville;

    public function __construct()
    {
        $this->BacManager = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getImageLien(): ?string
    {
        return $this->imageLien;
    }

    public function setImageLien(?string $imageLien): self
    {
        $this->imageLien = $imageLien;

        return $this;
    }

    /**
     * @return Collection|BacManager[]
     */
    public function getBacManager(): Collection
    {
        return $this->BacManager;
    }

    public function addBacManager(BacManager $bacManager): self
    {
        if (!$this->BacManager->contains($bacManager)) {
            $this->BacManager[] = $bacManager;
            $bacManager->setUser($this);
        }

        return $this;
    }

    public function removeBacManager(BacManager $bacManager): self
    {
        if ($this->BacManager->contains($bacManager)) {
            $this->BacManager->removeElement($bacManager);
            // set the owning side to null (unless already changed)
            if ($bacManager->getUser() === $this) {
                $bacManager->setUser(null);
            }
        }

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->CodePostal;
    }

    public function setCodePostal(int $CodePostal): self
    {
        $this->CodePostal = $CodePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->Ville;
    }

    public function setVille(string $Ville): self
    {
        $this->Ville = $Ville;

        return $this;
    }
    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        $bacJaune = new BacManager;
        $bacJaune->setActif(true)->setCouleur("jaune")->setDateDebut(new DateTime());
        $this->addBacManager($bacJaune);

        $bacGris = new BacManager;
        $bacGris->setActif(true)->setCouleur("gris")->setDateDebut(new DateTime());
        $this->addBacManager($bacGris);

        $bacVerre = new BacManager;
        $bacVerre->setActif(true)->setCouleur("vert")->setDateDebut(new DateTime());
        $this->addBacManager($bacVerre);
    }
}
