<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContenuRepository")
 */
class Contenu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $matiere;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BacManager", inversedBy="Contenu")
     */
    private $bacManager;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imageLien;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getMatiere(): ?string
    {
        return $this->matiere;
    }

    public function setMatiere(string $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getBacManager(): ?BacManager
    {
        return $this->bacManager;
    }

    public function setBacManager(?BacManager $bacManager): self
    {
        $this->bacManager = $bacManager;

        return $this;
    }

    public function getImageLien(): ?string
    {
        return $this->imageLien;
    }

    public function setImageLien(string $imageLien): self
    {
        $this->imageLien = $imageLien;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }
}
