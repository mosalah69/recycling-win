<?php

namespace App\Repository;

use App\Entity\BacManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BacManager|null find($id, $lockMode = null, $lockVersion = null)
 * @method BacManager|null findOneBy(array $criteria, array $orderBy = null)
 * @method BacManager[]    findAll()
 * @method BacManager[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BacManagerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BacManager::class);
    }

    // /**
    //  * @return BacManager[] Returns an array of BacManager objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BacManager
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
