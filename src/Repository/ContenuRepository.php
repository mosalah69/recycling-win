<?php

namespace App\Repository;

use App\Entity\Contenu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Contenu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contenu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contenu[]    findAll()
 * @method Contenu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContenuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contenu::class);
    }

    // /**
    //  * @return Contenu[] Returns an array of Contenu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contenu
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findByKeyword(string $keyword)
    {
 
    // on met les "%" avant et après le mot clé pour qu'il cherche ce qui contient mm si il y a des choses avant et apres
    // (doit etre fait à l'exterieur de la requette SQL)
        $key = '%'.$keyword.'%';
 
        $entityManager = $this->getEntityManager();
 
        // on fait une requette "FROM App\Entity..." et pas directement sur une Table SQL
        // il faut donc mettre un alias (ici "contenu"), pour qu'il la prenne en compte 
        // il va la considérer comme une "instance de la table" et chercher dedans.
 
        // là je le fait chercher dans le nom, la categorie et la description, parce qu'a priori c'est les endroits ou
        // un mot clé peut sortir
        $query = $entityManager->createQuery(
            'SELECT contenu
            FROM App\Entity\Contenu contenu
            WHERE contenu.Nom LIKE :key 
            OR contenu.matiere LIKE :key'
 
        )->setParameter('key', $key);
 
        // returns an array of Product objects
        return $query->getResult();
    }
}
