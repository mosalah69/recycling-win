<?php

namespace App\DataFixtures;

use App\Entity\Contenu;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
         $contenu = new Contenu();
         $contenu1 = new Contenu();
         $contenu2 = new Contenu();
         $contenu3 = new Contenu();
         $contenu4 = new Contenu();

         $contenu->setNom("Bouteille de lait");
         $contenu->setMatiere("Plastique");
         $contenu->setImageLien('/images/plastique.png');
         $contenu->setPrix(10);
         $manager->persist($contenu);

         $contenu1->setNom("Emballage");
         $contenu1->setMatiere("Carton");
         $contenu1->setImageLien("/images/carton.png");
         $contenu1->setPrix(15);
         $manager->persist($contenu1);

         $contenu2->setNom("Pot de confiture");
         $contenu2->setMatiere("Verre");
         $contenu2->setImageLien('/images/verre.png');
         $contenu2->setPrix(15);
         $manager->persist($contenu2);

         $contenu3->setNom("Pot en verre");
         $contenu3->setMatiere("Verre");
         $contenu3->setImageLien('/images/verre.png');
         $contenu3->setPrix(15);
         $manager->persist($contenu3);

         $contenu4->setNom("Yahourt");
         $contenu4->setMatiere("Déchets ménagers");
         $contenu4->setImageLien('/images/yahourt.png');
         $contenu4->setPrix(0);
         $manager->persist($contenu4);
        
        
         $manager->flush();
    }
}
